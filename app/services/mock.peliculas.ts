import {Pelicula} from "./../model/pelicula";

export const PELICULAS: Pelicula[] = [
	new Pelicula(1, "Batman v Superman", "Zack Snider", 2016),
	new Pelicula(2, "Star Wars", "George Lucas", 1972),
	new Pelicula(3, "Avengers", "Josh Weedon", 2012),
	new Pelicula(4, "Deadpool", "Tim Miller", 2016),
	new Pelicula(5, "Mad Max", "George Miller", 2015),
	new Pelicula(6, "The Matrix", "Watchowski Bros.", 1998)
];