System.register(["./../model/pelicula"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var pelicula_1;
    var PELICULAS;
    return {
        setters:[
            function (pelicula_1_1) {
                pelicula_1 = pelicula_1_1;
            }],
        execute: function() {
            exports_1("PELICULAS", PELICULAS = [
                new pelicula_1.Pelicula(1, "Batman v Superman", "Zack Snider", 2016),
                new pelicula_1.Pelicula(2, "Star Wars", "George Lucas", 1972),
                new pelicula_1.Pelicula(3, "Avengers", "Josh Weedon", 2012),
                new pelicula_1.Pelicula(4, "Deadpool", "Tim Miller", 2016),
                new pelicula_1.Pelicula(5, "Mad Max", "George Miller", 2015),
                new pelicula_1.Pelicula(6, "The Matrix", "Watchowski Bros.", 1998)
            ]);
        }
    }
});
//# sourceMappingURL=mock.peliculas.js.map