// Importar el núcleo de Angular
import {Component} from 'angular2/core';
import {OnInit} from "angular2/core";
import {Router, RouteParams} from "angular2/router";
import {Pelicula} from "./../model/pelicula";
import {PeliculasService} from "./../services/peliculas.services";

// Decorador component, indicamos en que etiqueta se va a cargar la plantilla
@Component({    
    templateUrl: "app/view/crear-pelicula.html", 
    providers: [PeliculasService]
 
})

// Clase del componente donde iran los datos y funcionalidades
export class CrearPeliculaComponent implements OnInit{ 
	public tituloPag:string	= "Crear Pelicula";
	public tituloPelicula:string = "";
	public nuevaPelicula:Pelicula;

	public constructor(private _peliculasService: PeliculasService, 
						private _router: Router,
						private _routerParams: RouteParams){

	}

	public onSubmit(){		

		this._peliculasService.insertPelicula(this.nuevaPelicula);

		//Redireccionamiento
		this._router.navigate(["Peliculas"]);
	}

	ngOnInit():any{
		this.tituloPelicula = this._routerParams.get("titulo");
		this.nuevaPelicula = new Pelicula(
			0,
			this._routerParams.get("titulo"), 
			this._routerParams.get("director"), 
			parseInt(this._routerParams.get("anio"))
			);
	}
	
}
